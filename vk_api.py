import requests
import time
import json
from configparser import ConfigParser

APP_ID = 0
API_VERSION = '5.74'
SECRET = ''


def main():
    code, access_token, expire_time, user_id = parse_config()
    if code is None:
        print('Config file corrupted, renew it please.')
        exit(1)
    if float(expire_time) < time.time():
        code, access_token, user_id = get_actual_data()

    commands = {'help': 'help', 'quit': 'quit', 'messages': 'messages.get', 'whoami': 'users.get',
                'my_info': 'account.getProfileInfo', 'get_status': 'status.get', 'custom': 'custom',
                'friends': get_friends, 'user_by_id': get_user_by_id}
    help = f'Available commands:\n{[q for q in commands.keys()]}\n'
    print(help)

    try:
        while True:
            method = input('>>> ')

            if method not in commands.keys():
                print('Invalid input\n')
                continue
            elif method == 'help':
                print(help)
                continue
            elif method == 'quit':
                exit(1)
            elif method == 'custom':
                custom_method = input('Enter your request here:\n\t>>> ')
                try:
                    print(beautify(send_request(custom_method, user_id, access_token)))
                except:
                    print('Not valid input\n')
                continue
            elif method in ['friends', 'user_by_id']:
                eval(commands[method].__name__ + f'(id, token)', {},
                     {commands[method].__name__: commands[method], 'id': user_id, 'token': access_token})
                continue

            print(beautify(send_request(commands[method], user_id, access_token)))
            continue

    except KeyboardInterrupt:
        print(' Goodbye!\n')
    except SystemExit:
        print(' Goodbye!\n')
    except:
        print(' Something went wrong, check your internet connection\n')


def get_friends(user_id, access_token):
    friends_ids = json.loads(send_request('friends.get', user_id, access_token).text)['response']['items']
    try:
        for id in friends_ids:
            print(beautify(send_request('users.get', id, access_token)))
            time.sleep(0.4)
    except KeyboardInterrupt:
        return


def get_user_by_id(user_id, access_token):
    id = input('Enter user id:\n')
    req = requests.get(f'https://api.vk.com/method/users.get?user_id={id}&access_token={access_token}&v={API_VERSION}')
    print(beautify(req))


def beautify(request):
    return str(request.status_code) + '\n' + str(
        json.dumps(json.loads(request.text), sort_keys=True, indent=2, ensure_ascii=False)) + '\n'


def send_request(method, user_id, access_token):
    return requests.get(
        f'https://api.vk.com/method/{method}?user_id={user_id}&access_token={access_token}&v={API_VERSION}')


def renew_config(code, access_token, expire_time, user_id):
    parser = ConfigParser(allow_no_value=True)
    parser['User'] = {'Code': code, 'Access_token': access_token, 'Expire_time': expire_time, 'ID': user_id}
    parser['App'] = {'SECRET': SECRET, 'APP_ID': APP_ID, 'API_VERSION': API_VERSION}
    with open('config.cfg', 'w+') as config:
        parser.write(config)


def get_actual_data():
    print('This app will ask you to grant access to your '
          'friends, photos, audio, video, status, docs, messages, groups\n')
    authorization = f'https://oauth.vk.com/authorize?client_id={APP_ID}' \
                    f'&display=popup' \
                    f'&redirect_uri=https://oauth.vk.com/blank.html' \
                    f'&scope=398366' \
                    f'&response_type=code' \
                    f'&v={API_VERSION}'
    code = input('Please, visit this page from your browser to sign in to your VK account '
                 'and enter the code you get: '
                 f'\n\t>>> {authorization}\n')
    getting_token = f'https://oauth.vk.com/access_token?client_id={APP_ID}' \
                    f'&client_secret={SECRET}' \
                    f'&redirect_uri=https://oauth.vk.com/blank.html' \
                    f'&code={code}'
    access_token = input('Please, enter token from this page: '
                         f'\n\t>>> {getting_token}\n')
    expire_time = time.time() + float(input('Enter expire time:\n'))
    user_id = input('Enter your ID:\n')
    renew_config(code, access_token, expire_time, user_id)
    return code, access_token, user_id


def parse_config():
    global SECRET
    global APP_ID
    global API_VERSION

    parser = ConfigParser(allow_no_value=True)
    try:
        with open('config.cfg', 'r+', encoding='utf-8') as file:
            parser.read_file(file)
        user = parser['User']
        app = parser['App']
        SECRET, APP_ID, API_VERSION = app['SECRET'], app['APP_ID'], app['API_VERSION']
        return user['Code'], user['Access_token'], user['Expire_time'], user['ID']
    except:
        return None, None, None, None


if __name__ == '__main__':
    main()
